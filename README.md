# M2 Logiciel project - AI

## Pre Requires

Please download this following libraries to run this project :

- [Zookeeper](https://www.apache.org/dyn/closer.cgi/zookeeper)
- [Kafka](https://kafka.apache.org/downloads)

Then please run : 

    ./apache-zookeeper-3.5.9-bin/bin/zkServer.sh start
    ./kafka_2.13-2.7.0/bin/kafka-server-start.sh -daemon ../config/server.properties

## Usage

To run this project we have to read the result.rdf file in resources.
A file is already generated but you can generate a new file with our program.

    // db.getModel
    // db.generatePeople()
    // db.generateVaccine()
    // db.toFile(Util.OUTPUT_FILE)

Uncomments this line if you want to generate new random information.

## Contributors

- Akram MALEK
- Violaine HUYNH
- Jean-Baptiste PILORGET
- Florent SIMONNOT