package utils

import scala.util.Random

/**
 * [SideEffect] provides a side effect structure
 */
trait SideEffect {
  /**
   * The name of side effect
   */
  val name: String
  /**
   * The side effect code
   */
  val sideCode: String
}

object SideEffect {

  val random = new Random()

  case object InjectionSitePain extends SideEffect {
    override val name: String = "injection site pain"
    override val sideCode: String = "C0151828"
  }

  case object Fatigue extends SideEffect {
    override val name: String = "fatigue"
    override val sideCode: String = "C0015672"
  }

  case object Headache extends SideEffect {
    override val name: String = "headache"
    override val sideCode: String = "C0018681"
  }

  case object MusclePain extends SideEffect {
    override val name: String = "muscle pain"
    override val sideCode: String = "C0231528"
  }

  case object Chills extends SideEffect {
    override val name: String = "chills"
    override val sideCode: String = "C0085593"
  }

  case object JointPain extends SideEffect {
    override val name: String = "joint pain"
    override val sideCode: String = "C0003862"
  }

  case object Fever extends SideEffect {
    override val name: String = "fever"
    override val sideCode: String = "C0015967"
  }

  case object InjectionSiteSwelling extends SideEffect {
    override val name: String = "injection site swelling"
    override val sideCode: String = "C0151605"
  }

  case object InjectionSiteRedness extends SideEffect {
    override val name: String = "injection site redness"
    override val sideCode: String = "C0852625"
  }

  case object Nausea extends SideEffect {
    override val name: String = "nausea"
    override val sideCode: String = "C0027497"
  }

  case object Malaise extends SideEffect {
    override val name: String = "malaise"
    override val sideCode: String = "C0231218"
  }

  case object Lymphadenopathy extends SideEffect {
    override val name: String = "lymphadenopathy"
    override val sideCode: String = "C0497156"
  }

  case object InjectionSiteTenderness extends SideEffect {
    override val name: String = "injection site tenderness"
    override val sideCode: String = "C0863083"
  }

  case object Null extends SideEffect {
    override val name: String = ""
    override val sideCode: String = ""
  }

}
