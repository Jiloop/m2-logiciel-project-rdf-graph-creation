package utils

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import kk.StreamUtils
import org.apache.kafka.streams.kstream.KStream

import java.text.SimpleDateFormat
import java.time.{LocalDate, Period, Year}
import java.time.format.DateTimeFormatter
import java.util.{Date, Locale}

object Util {
  /**
   * Input file path
   */
  val INPUT_FILE: String = "file:///Users/florentsimonnot/Dev/m2-logiciel-project-rdf-graph-creation/resources/lubm1.ttl"

  /**
   * Output file path
   */
  val OUTPUT_FILE: String = "./resources/result.rdf"

  /**
   * Profiles
   */
  val SUBJECTS: List[String] =
    "http://swat.cse.lehigh.edu/onto/univ-bench.owl#AssistantProfessor" ::
      "http://swat.cse.lehigh.edu/onto/univ-bench.owl#AssociateProfessor" ::
      "http://swat.cse.lehigh.edu/onto/univ-bench.owl#FullProfessor" ::
      "http://swat.cse.lehigh.edu/onto/univ-bench.owl#GraduateStudent" ::
      "http://swat.cse.lehigh.edu/onto/univ-bench.owl#Lecturer" ::
      "http://swat.cse.lehigh.edu/onto/univ-bench.owl#UndergraduateStudent" :: Nil

  /**
   * Company works on drugs
   */
  val DRUGS_COMPANY: List[String] = "Pfizer" :: "Moderna" :: "AstraZeneca" :: "SpoutnikV" :: "CanSinoBi" :: Nil

  /**
   * Vaccination distribution to population
   */
  val VACCINES_PERCENTAGE: List[Int] = 30 :: 22 :: 2 :: 15 :: 31 :: Nil

  /**
   * Percentage of man in the sample
   */
  val MALE_PERCENT: Int = 40

  /**
   * Percentage of female in the sample
   */
  val FEMALE_PERCENT: Int = 52

  /**
   * Percentage of people who are vaccinated in the sample
   */
  val PEOPLE_VACCINATED_PERCENT: Int = 69

  val BASE_URI = "http://extension.group1.fr/onto#"

  private val sdf = new SimpleDateFormat("dd/MM/yyyy")

  /**
   * Creates URI for a field
   * @param field - the field name (e.g. firstName)
   * @return the URI
   */
  def createUri(field: String): String = field match {
    case "firstName" => s"${BASE_URI}fName"
    case "lastName" => s"${BASE_URI}lName"
    case "gender" => s"${BASE_URI}gender"
    case "zipCode" => s"${BASE_URI}zipCode"
    case "birthdate" => s"${BASE_URI}birthdate"
    case "state" => s"${BASE_URI}state"
    case "id" => s"${BASE_URI}id"
    case "vaccine" => s"${BASE_URI}vaccine"
    case "vaccinationDate" => s"${BASE_URI}vaccinationDate"
  }

  implicit class tupleIntToTupleDate(t: (Int, Int)) {
    def createDate(age: Int): Date = age match {
      case 20 => sdf.parse("01/01/2001")
      case 30 => sdf.parse("01/01/1991")
      case 70 => sdf.parse("01/01/1951")
      case 1 => sdf.parse("15/01/2021")
      case 2 => sdf.parse("25/01/2021")
    }

    def intToDate: (Date, Date) = {
      (createDate(t._1), createDate(t._2))
    }
  }

  /**
   * Gets the vaccination date
   * @return
   */
  def getVaccinationDate(): (Date, Date) = {
    (1, 2).intToDate
  }

  /**
   * Generates the age
   * @param isOlder
   * @return
   */
  def generateAge(isOlder: Boolean): (Date, Date) = if (isOlder) {
    (30, 70).intToDate
  } else {
    (20, 30).intToDate
  }

  val mapper = new ObjectMapper()
  mapper.registerModule(DefaultScalaModule)

  /**
   * Date formatter
   */
  val format : DateTimeFormatter = DateTimeFormatter.ofPattern("EEE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH)

  private def isBetween(ld1: LocalDate, ld2: LocalDate, interval : Int): Boolean ={
    Period.between(ld1, ld2).getYears <= interval
  }

  /**
   * Checks if year is between an interval
   * @param year - the year
   * @param valueJson - the json value
   * @param interval - the interval
   * @return true if year is between the interval
   */
  def isBetween(year : Year, valueJson : String, interval : Int): Boolean = {
    val jsonTree = mapper.readTree(valueJson)
    jsonTree.has("birthdate") &&
      isBetween(
        LocalDate.parse(s"$year-01-01"),
        LocalDate.parse(jsonTree.get("birthdate").asText(), format),
        interval
      )
  }

  def everyIntervalOfYearsStream(source : KStream[String, String], years : List[Year], interval : Int) : Map[Year, KStream[String, String]] = {
    val map = collection.mutable.Map[Year, KStream[String, String]]()
    for (year <- years) {
      map.getOrElseUpdate(year, StreamUtils.filter(source, (_, value) =>  isBetween(year, value, interval)))
    }
    map.toMap
  }
}
