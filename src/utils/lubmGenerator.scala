package utils

import com.github.javafaker.Faker
import org.apache.jena.rdf.model.{Model, ModelFactory}
import java.io.{FileWriter, IOException}

import utils.SideEffect._

import scala.util.{Failure, Random, Success, Try}

class lubmGenerator(
                     val dbSource: String,
                     val maleToCheck: Int,
                     val vaccinationPercentToCheck: Int,
                     val vaccinesRepartitionToCheck: List[Int],
                     val vaccinesToCheck: List[String],
                     val subjects: List[String]
                   ) {

  val random = new Random()

  private def checkVaccineRepartition(vaccinesRepartitionToCheck: List[Int]): Try[List[Int]] = {
    vaccinesRepartitionToCheck.foreach { e =>
      if (e < 0 || e > 100) {
        Failure(throw new IllegalArgumentException("Each element of vaccinesRepartition should be between 0 and 100"))
      }
    }
    if (vaccinesRepartitionToCheck.size != vaccinesToCheck.size) Failure(throw new IllegalArgumentException("VaccinesRepartition size must be equals than vaccines"))
    if (vaccinesRepartitionToCheck.sum != 100) Failure(throw new IllegalArgumentException("VaccinesRepartition sum must be equals than 100"))

    Success(vaccinesRepartitionToCheck)
  }

  private def checkVaccines(vaccinesToCheck: List[String]): Try[List[String]] = {
    vaccinesToCheck.foreach { e =>
      if (e.isEmpty) {
        Failure(throw new IllegalArgumentException("Each element of vaccine should be non empty"))
      }
    }
    Success(vaccinesToCheck)
  }

  private def checkMale(maleToCheck: Int): Try[Int] = {
    if (maleToCheck > 100 || maleToCheck < 0) {
      Failure(throw new IllegalArgumentException("Mal percentage should be between 0 and 100"))
    }
    Success(maleToCheck)
  }

  private def checkVaccinationPercent(vaccinationPercentToCheck: Int): Try[Int] = {
    if (vaccinationPercentToCheck > 100 || vaccinationPercentToCheck < 0) {
      Failure(throw new IllegalArgumentException("vaccined percentage should be between 0 and 100"))
    }
    Success(vaccinationPercentToCheck)
  }

  val vaccinesRepartition: List[Int] = checkVaccineRepartition(vaccinesRepartitionToCheck).get
  val vaccines: List[String] = checkVaccines(vaccinesToCheck).get
  val male: Int = checkMale(maleToCheck).get
  val vaccinationPercent: Int = checkVaccinationPercent(vaccinationPercentToCheck).get

  val model: Model = ModelFactory.createDefaultModel()

  private val typeProperty = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type"
  private val rdfType = model.createProperty(typeProperty)

  implicit class modelImplicit(m: Model) {
    def addToModel(resource: String, uri: String, literal: String): Unit = {
      m.add(
        m.createResource(resource),
        m.createProperty(uri),
        m.createLiteral(literal)
      )
    }

    def addResourceToModel(resource: String, uri: String, uriResource: String): Unit = {
      m.add(
        m.createResource(resource),
        m.createProperty(uri),
        m.createResource(uriResource)
      )
    }
  }

  def getModel: Model = {
    model.read(dbSource, "TTL")
  }

  /**
   * created random data based on subjectType, and class parameters
   */
  def generatePeople() = {
    subjects.foreach { subjectType => {
      val iterator = model.listSubjectsWithProperty(rdfType, model.createResource(subjectType))

      val age = Util.generateAge(isABoomer(subjectType))

      while (iterator.hasNext) {
        val faker = new Faker(/*new Locale("en-US")*/)
        val uri = iterator.next().getURI
        model.addToModel(uri, Util.createUri("id"), faker.idNumber().toString)
        model.addToModel(uri, Util.createUri("firstName"), faker.name().firstName())
        model.addToModel(uri, Util.createUri("lastName"), faker.name.lastName())
        model.addResourceToModel(uri, Util.createUri("gender"), "http://extension.group1.fr/onto#" + randomGender())
        model.addToModel(uri, Util.createUri("zipCode"), faker.address.zipCode())
        model.addToModel(uri, Util.createUri("state"), faker.address.state())
        model.addToModel(uri, Util.createUri("birthdate"), faker.date().between(age._2, age._1).toString)
      }
    }
    }
  }

  /**
   * adds properties of vaccines to some person based on vaccinationPercent
   */
  def generateVaccine(): Unit = {
    subjects.foreach { subjectType => {
      val iterator = model.listSubjectsWithProperty(rdfType, model.createResource(subjectType))
      while (iterator.hasNext) {
        val uri = iterator.next().getURI
        if (isVaccinated()) {
          val faker = new Faker()
          val date = Util.getVaccinationDate()
          model.addResourceToModel(uri, Util.createUri("vaccine"), "http://extension.group1.fr/onto#" + randomVaccine())
          model.addToModel(uri, Util.createUri("vaccinationDate"), faker.date().between(date._1, date._2).toString)
        }
      }
    }
    }
  }

  /**
   * detects the age range of the person based on URI
   *
   * @param subjectType URI represents a person owning a property on his professional situation
   * @return
   */
  private def isABoomer(subjectType: String): Boolean = {
    val position = subjectType.indexOf("#")
    val profession = subjectType.substring(position)
    val professionsOlderThan30 = "#FullProfessor" :: "#AssociateProfessor" :: "#Lecturer" :: "#AssistantProfessor" :: Nil

    professionsOlderThan30.contains(profession)
  }

  /**
   * shooting at random according to the number of male percentage given in parameter the class
   *
   * @return Male or Female
   */
  private def randomGender(): String = {
    if (new Faker().number.numberBetween(0, 100) < male) "Male"
    else "Female"
  }

  /**
   * shooting at random according to the number of people percentage given in parameter the class
   *
   * @return true if the person is vaccinated, false otherwise
   */
  private def isVaccinated(): Boolean = {
    //Max=101 because max is exclusive
    new Faker().number.numberBetween(0, 101) < vaccinationPercent
  }

  /**
   * give a vaccine at random according to the probabilities given in parameter the class
   *
   * @return name of vaccine
   */
  private def randomVaccine(): String = {
    if (isVaccinated()) {
      Util.DRUGS_COMPANY(random.nextInt(Util.DRUGS_COMPANY.size))
    } else {
      ""
    }
  }

  /**
   * encode the model in rdf format
   *
   * @param fileName filename where the model will be encoded
   */
  def toFile(fileName: String): Unit = {
    val out = new FileWriter(fileName)
    try model.write(out, "RDF/XML-ABBREV")
    finally try out.close()
    catch {
      case closeException: IOException =>
      // ignore
    }
  }

  /**
   * Checks if vaccine has side effect
   * @param vaccine - the vaccine
   * @param percentage - an arbitrary percentage
   * @return true if vaccine has side effect
   */
  def hasSideEffect(vaccine: String, percentage: Int): Boolean = random.nextInt(100) <= percentage

  private def seqOf(): Seq[SideEffect] = Seq(
    InjectionSitePain, Fatigue, Headache, MusclePain, Chills, JointPain,
    Fever, InjectionSiteSwelling, InjectionSiteRedness, Nausea, Malaise,
    Lymphadenopathy, InjectionSiteTenderness
  )

  /**
   * Gets a random side effect for vaccine
   * Can return [SideEffect.Null] if the vaccine hasn't side effect
   * @param vaccine - the vaccine
   * @return a side effect as [SideEffect]
   */
  def randomSideEffect(vaccine: String): SideEffect = {
    if (hasSideEffect(vaccine, 39)) {
      val seq = seqOf()
      seq(random.nextInt(seq.size))
    } else {
      SideEffect.Null
    }

  }

  /**
   * Gets the vaccine
   *
   * @param url - the url
   * @return the name of vaccine
   */
  def findVaccine(url: String): String = {
    url match {
      case s"${Util.BASE_URI}Pfizer" => "Pfizer"
      case s"${Util.BASE_URI}Moderna" => "Moderna"
      case s"${Util.BASE_URI}AstraZeneca" => "AstraZeneca"
      case s"${Util.BASE_URI}SpoutnikV" => "SpoutnikV"
      case s"${Util.BASE_URI}CanSinoBio" => "CanSinoBio"
      case _ => ""
    }
  }

}
