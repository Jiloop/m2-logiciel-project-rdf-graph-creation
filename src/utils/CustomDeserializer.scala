package utils

import org.apache.kafka.common.serialization.{Deserializer, LongDeserializer, StringDeserializer}

/**
 * [CustomDeserializer] represents a custom deserializer
 */
class CustomDeserializer extends Deserializer[Any]{

  override def deserialize(topic: String, data: Array[Byte]): Any = {
    typeOf(topic) match {
      case StringData => new StringDeserializer().deserialize(topic, data)
      case LongData => new LongDeserializer().deserialize(topic, data)
    }
  }

  /**
   * The type of data to deserialize
   */
  trait TypeData

  case object LongData extends TypeData

  case object StringData extends TypeData

  /**
   * Will return the [[TypeData]] of the topic.
   *
   * @param topic name of the Topic.
   * @return the [[TypeData]].
   */
  def typeOf(topic: String): TypeData = {
    topic match {
      case x if (x == "stream-info-person-filter-output") => StringData
      case _ => LongData
    }
  }

}
