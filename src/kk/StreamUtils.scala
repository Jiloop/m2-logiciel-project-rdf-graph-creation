package kk

import java.util.Properties
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.streams.{KeyValue, StreamsConfig}
import org.apache.kafka.streams.kstream.{KStream, KeyValueMapper, Predicate, Produced, ValueJoiner}
import utils.Util._

object StreamUtils {
  val props: Properties = new Properties()
  props.put(StreamsConfig.APPLICATION_ID_CONFIG, "kafka_id")
  props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092")
  props.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0)
  props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String.getClass.getName)
  props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String.getClass.getName)
  props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")

  /**
   * Filters with predicate and return a stream with id as key.
   * @param stream the stream source
   * @param predicate the predicate
   * @return a new stream
   */
  def filter(stream: KStream[String, String], predicate: Predicate[String, String]): KStream[String, String] = {
    stream
      .filter(predicate)
      .map((_, value) => KeyValue.pair(mapper.readTree(value).get("id").asText(), value))
  }

  /**
   * Transforms the value of the stream with the valueTransform.
   * Keeps the Id as stream key.
   * @param source the stream source
   * @param valueTransform a function that receives two string and returns a string
   * @return a new stream
   */
  def transform(source: KStream[String, String], keyTransform: ((String, String) => String)): KStream[String, String] = {
    source.map((key, value) => KeyValue.pair(keyTransform(key, value), value))
  }

  /**
   * Joins two streams
   * @param kStream1 - the first stream
   * @param kStream2 - the second stream
   * @return a stream joined stream1 and stream2
   */
  def joinStream(kStream1: KStream[String, String], kStream2: KStream[String, String]): KStream[String, String] = {
    val kTable1 = kStream1.toTable
    val kTable2 = kStream2.toTable
    val joiner = new ValueJoiner[String, String, String] {
      override def apply(value1: String, value2: String): String = s"$value1/$value2"
    }
    kTable1.join(kTable2, joiner).toStream
  }

  /**
   * Groups the stream with keyValueMapper and count the number of occurrences.
   * @param countStream the stream to count occurrences
   * @param keyValueMapper - the key value mapper
   * @return a stream
   */
  def countStream[K, V, VR](countStream: KStream[K, V], keyValueMapper: KeyValueMapper[K, V, VR]): KStream[VR, java.lang.Long] = {
    val counts = countStream.
      groupBy(keyValueMapper)
      .count()
    counts.toStream
  }
}
