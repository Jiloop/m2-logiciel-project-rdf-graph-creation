package kk

import java.io.StringWriter
import java.util.Properties

import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.jena.rdf.model.Model
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.kafka.common.serialization.{Serdes, StringSerializer}
import org.apache.kafka.streams.StreamsConfig

object Producer {

  /***
   * Return the properties of a consumer.
   * Only done with StringSerializer for the moment.
   * @return Properties
   */
  def streamProducer: Properties = {
    val properties = new Properties
    properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "KafkaApp_id")
    properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092")
    properties.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0)
    properties.put("key.serializer", classOf[StringSerializer])
    properties.put("value.serializer", classOf[StringSerializer])
    properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String.getClass.getName)
    properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String.getClass.getName)
    properties
  }

  val producer = new KafkaProducer[String, String](streamProducer)

  /***
   * Will parse and send the value.
   * @param mapper will parse to a json
   * @param model the Model
   * @param value the ProducerValue
   */
  def send(mapper: ObjectMapper, model: Model, value : Map[String, String], destination: String): Unit = {
    val out: StringWriter = new StringWriter()
    mapper.writeValue(out, value)
    val json = out.toString
    //println(json)
    val record = new ProducerRecord[String, String](destination, "key", json)
    producer.send(record)
  }
}