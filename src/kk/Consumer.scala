package kk

import java.util
import java.util.Properties
import org.apache.kafka.clients.consumer.{ConsumerConfig, KafkaConsumer}
import org.apache.kafka.common.serialization.{Serdes, StringDeserializer}
import org.apache.kafka.streams.StreamsConfig
import utils.CustomDeserializer

object Consumer {

  /***
   * Return the properties of a consumer.
   * Only done with StringDeserializer for the moment.
   * @return Properties.
   */
  def streamConsumer: Properties = {
    val properties = new Properties
    properties.putIfAbsent(StreamsConfig.APPLICATION_ID_CONFIG, "KafkaApp_id")
    properties.putIfAbsent(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092")
    properties.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0)
    properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String.getClass.getName)
    properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String.getClass.getName)
    properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")
    properties.putIfAbsent("group.id", "mygroup")
    properties.put("key.deserializer", classOf[StringDeserializer])
    properties.put("value.deserializer", classOf[CustomDeserializer])
    properties
  }

  val consumer = new KafkaConsumer[String, Any](streamConsumer)

  /***
   * Display's all the consumers.
   */
  def consumeDisplay(observable: util.List[String]) : Unit = {
    consumer.subscribe(observable)
    val list = new util.ArrayList[Any]()
    while (true) {
      val consumerRecord = consumer.poll(100)
      if (consumerRecord.isEmpty) {
        return
      }
      consumerRecord.forEach((record) => {
        System.out.println("Record topic" + record.topic)
        System.out.println("Record Key " + record.key)
        System.out.println("Record value " + record.value)
        System.out.println("Record partition " + record.partition)
        System.out.println("Record offset " + record.offset)

      })
      consumer.commitAsync()
    }
  }
}