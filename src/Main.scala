import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import kk.{Consumer, Producer, StreamUtils}
import utils.{SideEffect, Util, lubmGenerator}
import load.Load
import org.apache.jena.rdf.model.ModelFactory

import java.util
import java.util.concurrent.CountDownLatch
import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.streams.{KafkaStreams, StreamsBuilder}
import org.apache.kafka.streams.kstream.{Consumed, KStream, Produced}

import java.time.Year

object Main extends App {
  val db = new lubmGenerator(
    Util.INPUT_FILE,
    Util.MALE_PERCENT,
    Util.PEOPLE_VACCINATED_PERCENT,
    Util.VACCINES_PERCENTAGE,
    Util.DRUGS_COMPANY,
    Util.SUBJECTS
  )

  // To generate RDF -> already done
  // db.getModel
  // db.generatePeople()
  //db.generateVaccine()
  // db.toFile(Util.OUTPUT_FILE)

  // Read rdf file in resources
  val modelRDF = ModelFactory.createDefaultModel()
  modelRDF.read("file:resources/result.rdf", "RDF/XML-ABBREV")

  // Creates JSON mapper will be used for the producer.
  val mapper = new ObjectMapper()
  mapper.registerModule(DefaultScalaModule)

  // Method will load the file using blaze then entering the query.
  // The last parameter will be a function that takes the results of the query and returns Unit
  Load.load(
    "/result.rdf", "select ?id ?firstName ?lastName ?vaccinationDate ?o where {" +
      s"?s <${Util.createUri("vaccine")}> ?o." +
      s"?s <${Util.createUri("id")}> ?id." +
      s"?s <${Util.createUri("firstName")}> ?firstName." +
      s"?s <${Util.createUri("lastName")}> ?lastName." +
      s"?s <${Util.createUri("vaccinationDate")}> ?vaccinationDate. }",
    result => {
      try while ( {
        result.hasNext
      }) {
        // will send a ProducerValue if there's a sideEffect see Vaccine for more details.
        val bs = result.next
        val id = bs.getValue("id").stringValue()
        val firstName = bs.getValue("firstName").stringValue()
        val lastName = bs.getValue("lastName").stringValue()
        val vaccinationDate = bs.getValue("vaccinationDate").stringValue()
        val obj = bs.getValue("o").stringValue()
        val vaccine = db.findVaccine(obj)
        val sideEffect = db.randomSideEffect(vaccine)
        if (sideEffect != SideEffect.Null) {
          Producer.send(mapper, modelRDF, Map(
            "id" -> id,
            "fName" -> firstName,
            "lName" -> lastName,
            "vaccinationDate" -> vaccinationDate,
            "vaccine" -> vaccine,
            "sideEffectName" -> sideEffect.name,
            "sideEffectCode" -> sideEffect.sideCode
          ),
            "stream-info-person-input"
          )
        }
      }
      finally result.close()
    }
  )

  // Method will load the file using blaze then entering the query.
  // The last parameter will be a function that takes the results of the query and returns Unit
  Load.load("/result.rdf", "select ?id ?birthday ?o where {" +
    s"?s <${Util.createUri("vaccine")}> ?o." +
    s"?s <${Util.createUri("id")}> ?id." +
    s"?s <${Util.createUri("birthdate")}> ?birthday." +
    "}",
    result => {
      try while ( {
        result.hasNext
      }) {
        val bs = result.next
        val id = bs.getValue("id").stringValue()
        val birthday = bs.getValue("birthday").stringValue()
        val obj = bs.getValue("o").stringValue()
        val vaccine = db.findVaccine(obj)
        val sideEffect = db.randomSideEffect(vaccine)
        if (sideEffect != SideEffect.Null) {
          Producer.send(mapper, modelRDF, Map(
            "id" -> id,
            "birthday" -> birthday,
            "vaccine" -> vaccine,
            "sideEffectName" -> sideEffect.name,
            "sideEffectCode" -> sideEffect.sideCode
          ),
            "stream-sideEffect-input"
          )
        }
      }
      finally result.close()
    }
  )

  val builder = new StreamsBuilder()
  val source1: KStream[String, String] = builder.stream("stream-info-person-input", Consumed.`with`(Serdes.String, Serdes.String))
  val source2: KStream[String, String] = builder.stream("stream-sideEffect-input", Consumed.`with`(Serdes.String, Serdes.String))
  val source: KStream[String, String] = source1.merge(source2)

  val sideEffectsStream = StreamUtils.transform(source,
    (_, value) => s"${mapper.readTree(value).get("sideEffectCode")}"
  )

  val perVaccine = StreamUtils.transform(
    sideEffectsStream,
    (key, value) => s"$key/${mapper.readTree(value).get("vaccine")}")

  val joinStream = StreamUtils.joinStream(sideEffectsStream, perVaccine)

  val every = Util.everyIntervalOfYearsStream(source, List(Year.of(1950), Year.of(1960), Year.of(1970), Year.of(1980), Year.of(1990), Year.of(2000)), 10)

  for (y <- 1950 to 2000 by 10) {
    val stream = StreamUtils.joinStream(joinStream, every.get(Year.of(y)).get)
    val counts = StreamUtils.countStream(stream,
      (key: String, _: String) => {
        val array = key.split("/").toList
        array(1)
      })

    counts.to(s"counts-output-$y", Produced.`with`(Serdes.String, Serdes.Long))
  }


  val streams = new KafkaStreams(builder.build, StreamUtils.props)

  val latch: CountDownLatch = new CountDownLatch(1)

  // attach shutdown handler to catch control-c
  Runtime.getRuntime.addShutdownHook(new Thread("streams-shutdown-hook") {
    override def run(): Unit = {
      streams.close()
      latch.countDown()
    }
  })


  try {
    streams.start()
    latch.await()
  } catch {
    case _: Throwable =>
      System.exit(1)
  }
  Consumer.consumeDisplay(util.Arrays.asList(
    "stream-info-person-filter-output",
    "counts-output-1950",
    "counts-output-1960",
    "counts-output-1970",
    "counts-output-1980",
    "counts-output-1990",
    "counts-output-2000",
  ))

  Producer.producer.close()

  Consumer.consumer.close()
}
